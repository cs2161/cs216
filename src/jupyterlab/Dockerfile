# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# Ubuntu 18.04 (bionic) from 2018-05-26
# https://github.com/docker-library/official-images/commit/aac6a45b9eb2bffb8102353c350d341a410fb169
#ARG BASE_CONTAINER=ubuntu:bionic-20180526@sha256:c8c275751219dadad8fa56b3ac41ca6cb22219ff117ca98fe82b42f24e1ba64e
#FROM $BASE_CONTAINER
FROM ubuntu:bionic 
LABEL maintainer="Jupyter Project <jupyter@googlegroups.com>"
ARG NB_USER="jovyan"
ARG NB_UID="1000"
ARG NB_GID="100"

RUN echo 'force complete rebuild'
USER root

RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates 


#### apache arrow needed for pyarrow
RUN wget -O /usr/share/keyrings/red-data-tools-keyring.gpg https://packages.red-data-tools.org/ubuntu/red-data-tools-keyring.gpg
RUN echo 'deb [signed-by=/usr/share/keyrings/red-data-tools-keyring.gpg] https://packages.red-data-tools.org/ubuntu/ bionic universe' >> /etc/apt/sources.list.d/red-data-tools.list
RUN echo 'deb-src [signed-by=/usr/share/keyrings/red-data-tools-keyring.gpg] https://packages.red-data-tools.org/ubuntu/ bionic universe' >> /etc/apt/sources.list.d/red-data-tools.list


# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
    fonts-liberation \
    build-essential \
    emacs \
    git \
    vim \
    inkscape \
    jed \
    libsm6 \
    libxext-dev \
    libxrender1 \
    lmodern \
    netcat \
    pandoc \
    python-dev \
    texlive-fonts-extra \
    texlive-fonts-recommended \
    texlive-generic-recommended \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-xetex \
    unzip \
    nano \
    krb5-multidev \
    python-setuptools \
    pkg-config \
    apt-transport-https \
    libarrow-dev \
    libarrow-glib-dev \
    lsb-core \
    gfortran \
    libopenblas-dev \
    liblapack-dev \
    ffmpeg && apt-get clean && rm -rf /var/lib/apt/lists/*
 

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Configure environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    NB_USER=$NB_USER \
    NB_UID=$NB_UID \
    NB_GID=$NB_GID \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8
ENV PATH=$CONDA_DIR/bin:$PATH \
    HOME=/home/$NB_USER

ADD fix-permissions /usr/local/bin/fix-permissions
# Create jovyan user with UID=1000 and in the 'users' group
# and make sure these dirs are writable by the `users` group.
RUN groupadd wheel -g 11 && \
    echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su && \
    useradd -m -s /bin/bash -N -u $NB_UID $NB_USER && \
    mkdir -p $CONDA_DIR && \
    chown $NB_USER:$NB_GID $CONDA_DIR && \
    chmod g+w /etc/passwd && \
    fix-permissions $HOME && \
    fix-permissions $CONDA_DIR

USER $NB_USER

# Setup work directory for backward-compatibility
RUN mkdir /home/$NB_USER/work && \
    fix-permissions /home/$NB_USER

# Install conda as jovyan and check the md5 sum provided on the download site
ENV MINICONDA_VERSION=4.6.14 \
    CONDA_VERSION=4.7.10
RUN cd /tmp && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
    echo "718259965f234088d785cad1fbd7de03 *Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh" | md5sum -c - && \
    /bin/bash Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Miniconda3-${MINICONDA_VERSION}-Linux-x86_64.sh && \
    $CONDA_DIR/bin/conda config --system --prepend channels conda-forge && \
    $CONDA_DIR/bin/conda config --system --set auto_update_conda false && \
    $CONDA_DIR/bin/conda config --system --set show_channel_urls true && \
    $CONDA_DIR/bin/conda install --quiet --yes conda="${MINICONDA_VERSION%.*}.*" && \
    $CONDA_DIR/bin/conda update --all --quiet --yes && \
    conda clean -tipsy && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN conda update -n base conda 

# Install Tini
RUN conda install --quiet --yes 'tini=0.18.0' && \
    conda list tini | grep tini | tr -s ' ' | cut -d ' ' -f 1,2 >> $CONDA_DIR/conda-meta/pinned && \
    conda clean -tipsy && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

USER root
RUN apt-get update && apt-get install -yq \
      nodejs \
      npm

USER $NB_USER
#RUN conda install -c conda-forge nodejs
# Install Jupyter Notebook, Lab, and Hub
# Generate a notebook server config
# Cleanup temporary files
# Correct permissions
# Do all this in a single RUN command to avoid duplicating all of the
# files across image layers when the permissions change
#RUN conda install --quiet --yes \
#    'notebook=6.1.6' \
#    'jupyterhub=1.3.0' \
#    'jupyterlab=2.2.9' && \
#    conda clean --all -f -y && \
RUN conda install --quiet --yes \
    'jedi=0.17.2' \
    'notebook=6.2.0' \
    'jupyterhub=1.3.0' \
    'jupyterlab=2.2.9' && \
    conda clean --all -f -y && \
    npm cache clean --force && \
    jupyter notebook --generate-config && \
    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER
	

#    'ipywidgets>=7.5.1' \
#    'ipython=7.8.0' \
#    'ipykernel=5.1.2' && \

USER root

EXPOSE 8888
WORKDIR /home/$NB_USER/work

#-------- bash kernel
# add bash kernel for the user jovyan
USER $NB_USER
RUN pip install  bash_kernel
RUN python -m bash_kernel.install

RUN pip install pyarrow
RUN pip install xgboost
RUN pip install nose

# Install Python 3 packages
# Remove pyqt and qt pulled in for matplotlib since we're only ever going to
# use notebook-friendly backends in these images
RUN conda install -c conda-forge --yes \
    'conda-forge::blas=*=openblas' 

RUN conda install -c conda-forge --yes \
    'pandas' \
    'numexpr' \
    'matplotlib' \
    'scipy' \
    'seaborn' 

RUN conda install -c conda-forge --yes \
    'scikit-learn' \
    'scikit-image' \
    'sympy' \
    'cython' \
    'patsy' \
    'statsmodels' \
    'cloudpickle' \
    'dill' \
    'numba' 

RUN conda install -c conda-forge --yes \
    'bokeh' \
    'sqlalchemy' \
    'hdf5' \
    'h5py' \
    'vincent' \
    'beautifulsoup4' \
    'protobuf' \
    'xlrd' 

RUN conda install --yes \
     'mock' \
     'pandas' \
     'numpy' \
     'imageio' \
     'tqdm' 

# Import matplotlib the first time to build the font cache.
ENV XDG_CACHE_HOME /home/$NB_USER/.cache/
RUN MPLBACKEND=Agg python -c "import matplotlib.pyplot" 

# support for matplotlib and bokeh
RUN conda install -c conda-forge --yes \
    ipympl \
    phantomjs \
    pillow \
    selenium 

# Jupyterlab extensions support
#RUN conda install -c conda-forge nodejs
#RUN jupyter labextension install  @jupyter-widgets/jupyterlab-manager@1.0
RUN jupyter labextension install jupyter-matplotlib
#RUN jupyter labextension install jupyterlab_bokeh
#RUN conda install -c conda-forge --yes jupyterlab_bokeh
RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager
#RUN jupyter labextension install @bokeh/jupyter_bokeh

# altair
RUN conda install --yes \
    'altair' \
    'xlrd' 

RUN jupyter labextension install @jupyterlab/vega3-extension

RUN conda install -c pyviz  --yes \
    'holoviews' \ 
    'geoviews' \
    'param' \
    'panel' 
	
# pytorch
RUN conda install  -c pytorch pytorch torchvision torchtext cpuonly  --yes 

	
RUN  conda clean --all -f --yes  && \
     rm -rf /home/$NB_USER/.cache/yarn 
RUN  fix-permissions $CONDA_DIR && \
     fix-permissions /home/$NB_USER

RUN pip install gym 

####
# HACK FOR GYM HEADLESS RENDERING
####
USER root
RUN apt-get install -y freeglut3-dev
RUN apt-get install -y python-opengl
RUN apt-get install -y xvfb

####
# end HACK FOR GYM HEADLESS RENDERING
####

# Configure container startup
ENTRYPOINT ["tini", "-g", "--"]
CMD ["start-notebook.sh"]

#####
# Add local files as late as possible to avoid cache busting
USER root

COPY start.sh /usr/local/bin/
COPY start-notebook.sh /usr/local/bin/
COPY start-singleuser.sh /usr/local/bin/
COPY start-vrfb-lab.sh /usr/local/bin/
COPY start-vrfb-notebook.sh /usr/local/bin/
COPY jupyter_notebook_config.py /etc/jupyter/
RUN fix-permissions /etc/jupyter/


# Switch back to jovyan to avoid accidental container runs as root
#USER $NB_USER


